# Ubuntu Provisioning Scripts
First pass on ubuntu provisioning script for desktop machines

## Usage
```bash
./setup.sh
```
## License
Unlicensed, © 2017 [Spinifex Group](https://spinifexgroup.com/)
