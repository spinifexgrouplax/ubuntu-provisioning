#!/bin/bash
sudo -v
# prompt the user for root creds
while true; do sudo -n true; sleep 60; kill -0 "$$" || exit; done 2>/dev/null &

# install ntp, set timezone
echo "installing ntp, setting timezone"
sudo apt-get -qq update
sudo apt-get -qq install ntp -y
sudo timedatectl set-timezone America/Los_Angeles

# install system deps
# also includes a workaround for electron playback on linux
# workaround for https://github.com/electron/electron/issues/1518
sudo apt-get -qq install \
    apt-transport-https \
    libgconf-2-4 \
    ca-certificates \
    curl \
    exfat-fuse \
    exfat-utils \
    ncdu \
    build-essential \
    collectd \
    openssh-server \
    software-properties-common -y

# teamviewer
teamviewer="/usr/bin/teamviewer"
if [ -f "$teamviewer" ]
then
	echo "teamviewer installed, skipping..."
else
	echo "teamviewer NOT installed, installing..."
	sudo wget https://download.teamviewer.com/download/teamviewer_i386.deb
    sudo dpkg -i teamviewer_i386.deb
    sudo apt-get install -yf
    sudo rm teamviewer_i386.deb
fi

intel="/usr/bin/intel-graphics-update-tool"
if [ -f "$intel" ]
then
	echo "intel graphics updater installed, skipping..."
else
    # intel update tool
    echo "installing intel graphics updater tool v2.0.6..."
    sudo wget https://download.01.org/gfx/ubuntu/17.04/main/pool/main/i/intel-graphics-update-tool/intel-graphics-update-tool_2.0.6_amd64.deb
    sudo wget https://download.01.org/gfx/RPM-GPG-GROUP-KEY-ilg
    sudo apt-key add RPM-GPG-GROUP-KEY-ilg
    sudo rm RPM-GPG-GROUP-KEY-ilg
    sudo dpkg -i intel-graphics-update-tool_2.0.6_amd64.deb
    sudo rm intel-graphics-update-tool_2.0.6_amd64.deb
fi

echo "running update after intel graphics updater install..."
sudo apt-get -qq update
sudo apt-get -qq upgrade -y

echo "resetting all settings to default..."
dconf reset -f /

echo "changing power management settings..."
gsettings set org.gnome.settings-daemon.plugins.power sleep-inactive-ac-timeout 0
gsettings set org.gnome.settings-daemon.plugins.power sleep-inactive-battery-timeout 0
gsettings set org.gnome.desktop.session idle-delay 0
gsettings set org.gnome.settings-daemon.plugins.power idle-dim false
gsettings set org.gnome.desktop.lockdown disable-lock-screen true
gsettings set org.gnome.desktop.background primary-color "#000000"
gsettings set org.gnome.desktop.background color-shading-type "solid"

echo "disabling screensaver..."
xset s off
echo "xset s off" | tee ~/.xsession

echo "removing autostarts..."
# check for autostart first, then remove all userland auto start files
sudo rm -rf ~/.config/autostart && mkdir ~/.config/autostart

sudo /usr/bin/intel-graphics-update-tool
